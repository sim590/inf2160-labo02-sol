------------
--  pgcd  --
------------
pgcd :: Int -> Int -> Int
pgcd a b
    | a == 0 || b == 0 = maxi
    | otherwise        = pgcd (maxi `mod` mini) mini
    where
        mini = min a b
        maxi = max a b

-- vim:set et sw=4 ts=4 tw=80:

