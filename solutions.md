# Labo 2: Solutions

On commence premièrement par envoyer les instructions suivantes afin de charger
le module `ExerciceListe.hs` et les bibliothèques `Data.Char` et `Data.List`:

```
Prelude> import Data.Char
Prelude Data.Char> import Data.List
Prelude Data.Char Data.List> :l ExerciceListe.hs
[1 of 1] Compiling Main             ( ExerciceListe.hs, interpreted )
Ok, modules loaded: Main.
```

## 2 - Manipulation de listes

> (`sort`) Triez la liste `nombres` (disponible dans `ExerciceListe.hs`).

```
*Main Data.Char Data.List> sort nombres
[1,1,8,12,14,15,17,23,24,32,35,37,42,43,46,53,54,54,59,67,70,71,72,76,79,80,82,83,86,86,87,92,99,111,115,117,117,127,130,131,131,132,132,133,135,138,141,142,143,144,145,147,152,154,155,156,157,160,161,163,167,171,175,175,189,189,192,193,193,199,202,202,203,212,214,214,219,221,223,226,227,228,230,230,231,232,234,236,241,243,249,250,255,265,271,273,275,276,278,280,282,284,288,289,291,305,308,310,314,319,319,320,321,327,327,328,331,332,338,339,340,342,342,343,343,344,345,346,350,350,352,356,358,360,360,361,363,365,367,368,369,369,370,378,380,383,384,384,385,390,391,393,394,396,400,400,403,407,410,424,425,426,436,436,437,438,440,442,444,449,450,451,453,456,460,462,462,462,466,467,473,475,475,478,479,479,480,480,481,483,484,485,487,492,493,494,494,497,499,499]
```

> (`nub`) Transformez la liste `nombres` de sorte qu'elle ne contienne aucun
doublon.

```
*Main Data.Char Data.List> nub nombres
[243,456,438,17,481,234,284,193,462,360,127,350,276,390,192,83,400,475,484,479,115,342,156,467,345,339,236,152,288,314,460,232,319,273,130,175,171,160,87,369,144,493,219,340,480,499,403,53,385,310,32,157,394,141,451,485,82,42,383,111,278,214,327,212,70,271,72,15,289,131,230,145,54,338,483,255,494,365,37,221,282,23,407,367,396,135,249,487,497,478,143,67,450,35,86,305,199,79,76,358,80,8,132,370,147,368,142,275,203,346,466,24,410,43,117,291,425,202,436,332,331,343,449,231,227,167,1,363,442,228,328,92,437,14,308,492,361,321,444,280,133,226,138,393,380,46,378,344,59,163,320,391,189,161,241,426,424,250,12,384,473,352,155,265,356,99,453,440,223,71,154]
```

> (`minimum`, `maximum`) Calculez l'étendue de la liste `nombres`. En
statistiques, l'*étendue* d'une liste de nombre est la différence entre la
valeur maximale et la valeur minimale.

```
*Main Data.Char Data.List> maximum nombres - minimum nombres
498
```

> (`sum`, `realToFrac`, `genericLength`) Calculez la moyenne de `nombres`. Il
peut être bon de vérifier d'abord pourquoi ça ne fonctionne pas en utilisant
seulement `sum` et `length`.

```
*Main Data.Char Data.List> realToFrac  (sum nombres) / realToFrac  (length nombres)
271.29
```

ou bien plus court:
```
*Main Data.Char Data.List> realToFrac (sum nombres) / genericLength nombres
271.29
```

> (`odd`, `filter`, `take`) Extraire les 10 premiers nombres de la liste
`nombres` qui sont impairs.

```
*Main Data.Char Data.List> filter odd (take 10 nombres)
[243,17,481,193]
```

> (`mod`, `div`) Produisez le couple `(q,r)` où `q` est le quotient et `r` le
reste de la division de `147457` par `1297`.

```
*Main Data.Char Data.List> (147457 `div` 1297, 147457 `mod` 1297)
(113,896)
```

> (`sum`) Montrez que la somme des entiers de 0 à 100 est égale à `100 * 101 / 2`.

```
*Main Data.Char Data.List> sum [0..100] == 100 * 101 / 2
True
```

> (`length`, `permutations`, `factorial`) Montrez que le nombre de permutations
de la liste `[1..8]` est bien égal à `8!` (le point d'excalamation se lit
"factoriel").

```
*Main Data.Char Data.List> length (permutations [1..8]) == factorial 8
True
```

> (`transpose`) Montrez que la matrice `matrice` est symétrique (les valeurs
sont invariantes quand on applique une réflexion par rapport à la diagonale).

```
*Main Data.Char Data.List> transpose matrice == matrice
True
```

> (`toLower`) Transformez `phrase` en lettre minuscule.

~~Ici, On doit utiliser `map` en plus de `toLower`~~

L'idée initiale serait de penser qu'on soit forcé d'utiliser `map` comme suit:

```
*Main Data.Char Data.List> map toLower  phrase
"was it a car or a cat i saw?"
```

Comme @angetato le [fait remarquer][remarque-ange], on peut utiliser la
compréhension de liste sans `map`:

```
*Main Data.Char Data.List> [ toLower x | x <- phrase]
"was it a car or a cat i saw?"
```

[remarque-ange]: https://gitlab.com/angetato/INF2160_H2018_Solutions/blob/master/Labo2_solution.md

> (`toLower`, `filter`, `map`, `isLower`) Transformez `phrase` en minuscule et
en supprimant les espaces et les signes de ponctuation.

```
*Main Data.Char Data.List> filter isLower (map toLower phrase)
"wasitacaroracatisaw"
```

*Remarque*: Vous pouvez réduire le nombre de caractères en retirant les
parenthèses et en optant plutôt pour le symbole de dollar (`$`):

```
*Main Data.Char Data.List> filter isLower $ map toLower phrase
"wasitacaroracatisaw"
```

> (`toLower`, `filter`, `map`, `isLower`, `reverse`) Montrez que `phrase` est
une phrase palindromique, c'est-à-dire qu'on obtient la même phrase qu'on la
lise de gauche à droite ou de droite à gauche, en supposant qu'on ignore la
casse et les signes de ponctuation.

```
*Main Data.Char Data.List> (reverse . filter isLower $ map toLower phrase)  == (filter isLower $ map toLower phrase)
True
```

Sur deux lignes, la chose suivante élimine la redondance:
```
*Main Data.Char Data.List> let minphrase = filter isLower $ map toLower phrase
*Main Data.Char Data.List> minphrase == reverse minphrase
True
```

> (`filter`, `isSpace`, `length`) Compter le nombre d'espaces dans `phrase`.

```
*Main Data.Char Data.List> length $ filter isSpace phrase
8
```

> (`filter`, `isUpper`, `length`) Compter le nombre de lettres majuscules dans
`phrase`.

```
*Main Data.Char Data.List> length $ filter isUpper  phrase
2
```

> (`all`, `take`, `length`, `drop`, `isUpper`, `isAlpha`, `isDigit`) Montrez
que `code` est bien un code permanent valide (4 lettres majuscules, suivies
de 8 chiffres).

```
*Main Data.Char Data.List> length code == 12 && (all (\x -> isAlpha c && isUpper c) $ take 4 code) && (all isDigit $ drop 4 code)
True
```


## 3 - Définition de fonctions

### 3.1 - La fonction pgcd

Voir le fichier `pgcd.hs`.

> Implémentez la fonction qui calcule le plus grand commun diviseur de deux
> entiers. Sa signature est
> ```haskell
> pgcd :: Int -> Int -> Int
> ```
> *Remarque*: Mathématiquement, *pgcd(0,0)* n'est pas défini, mais vous pouvez
> supposer que *pgcd(0,0) = 0* pour simplifier.

L'algorithme utilisé est [celui-ci][algo].  Premièrement, si `a` ou `b` est
nul, alors soit un des deux est nul et on doit retourner celui qui n'est pas
nul ou bien les deux sont nuls et on retounrne 0 tout simplement.

```haskell
pgcd a b
    | a == 0 || b == 0 = maxi
    | otherwise        = pgcd (maxi `mod` mini) mini
    where
        mini = min a b
        maxi = max a b
```

Sinon, le cas général (algorithme d'euclide): Si `a > b` (par exemple), on doit
calculer `pgcd(a mod b, b)`. Ainsi, ce prochain appel à pgcd retournera le
`pgcd(r, b)` où `r = (a mod b) < a` car c'est le reste de la division. Ainsi,
les opérandes de part et d'autre vont diminuer vers 0 et nous renvoyer
éventuellement vers le cas de base. Vous pouvez tester comme suit:

```
*Main Data.Char Data.List> pgcd 3 12
3
*Main Data.Char Data.List> pgcd 33 122
1
*Main Data.Char Data.List> pgcd 45 120
15
```

[algo]: https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Algorithme_PGCD.svg/440px-Algorithme_PGCD.svg.png

## 3.2 - Périodes d'une liste

Voir le fichier `periode.hs`.

> Soit `L` une liste de longueur `n` et `p` un entier tel que `0 < p < n`. On dit
> que `p` est une *période* de `L` si `L[i] = L[i+p]` pour tout indice `i` valide
> dans `L` tel que `L[i+p]` est aussi un indice valide.
>
> Implémentez une fonction qui indique si un nombre est une période d'une liste,
> dont la signature est
> ```haskell
> estPeriode :: Eq a => Int -> [a] -> Bool
> ```
> *Remarque*: Vous pouvez retourner systématiquement `False` si la période
> indiquée n'est pas entre `1` et `n - 1`.
>
> Par exemple, on s'attend au comportement suivant:
> ```haskell
> Prelude> estPeriode 3 [1,2,3,1,2]
> True
> Prelude> estPeriode 0 [1,2,3,1,2,3]
> False
> Prelude> estPeriode 4 [1,2,3,1,1]
> True
> ```

On couvre premièrement les cas de base. Si la période est nulle, alors c'est
faux par définition comme décrit dans le code ci-après:

```haskell
estPeriode p list
        | p <= 0 || p >= length list = False
        | otherwise = estPeriode_ p list
```

Le cas `p >= length list` doit être géré indépendemment de la définition
suivante (*notez* les fonctions `estPeriode` et `estPeriode_`). En effet, ce cas
ne peut être géré dans `estPeriode_` car il s'agit d'un cas valide dans la
définition récursive ci-après.

```haskell
estPeriode_ p (x:xs)
        | length (x:xs) <= p = True
        | otherwise = all (==x) (sub (x:xs)) && estPeriode_ p xs
    where sub xss = [s | (s,i) <- (zip xss [0..]), i `mod` p == 0]
```

Le premier cas est la condition d'arrêt. En effet, dans le second cas, la liste
rétrécira jusqu'à ce que le premier cas soit satisfait. Dans le second cas, on
vérifie grâce à `all` que tous les nombres `L[i+p]` à des indices `i+p` valides
sont égaux. Finalement, on procède avec le prochain élément avec un appel
récursif à `estPeriode_`.

Il est cependant encore possible d'améliorer! En effet, selon @ablondin (voir le
[commentaire][commentaire-ablondin]), on peut utiliser la fonction `zipWith` et
écrire plutôt:

```haskell
estPeriode_ :: Eq a => Int -> [a] -> Bool
estPeriode_ p l = and $ zipWith (==) l (drop p l)
```

Cela donne quelque chose de beaucoup plus concis comme le fait remarquer
@ablondin.

[commentaire-ablondin]: https://gitlab.com/sim590/inf2160-labo02-sol/commit/ab78302b74c0502f32ee0c5185275022bd076845#note_55450099

>
> Ensuite, implémentez une fonction
> ```haskell
> periodes :: Eq a => [a] -> [Int]
> ```
> qui retourne la liste en ordre croissant de toutes les périodes d'une liste. On
> s'attend donc au comportement suivant:
> ```haskell
> Prelude> periodes [1,2,3,1,2]
> [3]
> Prelude> periodes "aaaaa"
> [1,2,3,4]
> Prelude> periodes [0,1,0,0,1,0,1,0,0,1,0]
> [5,8,10]
> ```

On utilise ici la compréhension de liste en parcourant tous les candidats
possibles et en filtrant à l'aide de la fonction `estPeriode` précédemment
définie.

```haskell
periodes liste = [p | p <- [1..length liste-1], estPeriode p liste]
```

Aussi, on peut [écrire][periodes-ablondin] comme suit:

```haskell
periodes liste = filter ((flip estPeriode) liste) [1..length liste - 1]
```

[periodes-ablondin]: https://gitlab.com/sim590/inf2160-labo02-sol/commit/ab78302b74c0502f32ee0c5185275022bd076845#note_55450242

<!-- vim:set et sw=4 ts=4 tw=80: -->

