
LABO2 := inf2160-exercices/labo02/
FILES := ExerciceListe.hs

all: prepare-files

prepare-submodule:
	git submodule update --init

prepare-files: prepare-submodule
	$(foreach f,$(FILES),$(shell ln -fs $(LABO2)/$(f)))

# vim:set noet sts=0 sw=4 ts=4 tw=80:

