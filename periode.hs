------------------
--  estPeriode  --
------------------
estPeriode_ :: Eq a => Int -> [a] -> Bool
estPeriode_ p (x:xs)
        | length (x:xs) <= p = True
        | otherwise = all (==x) (sub (x:xs)) && estPeriode_ p xs
    where sub xss = [s | (s,i) <- (zip xss [0..]), i `mod` p == 0]
estPeriode p list
        | p <= 0 || p >= length list = False
        | otherwise = estPeriode_ p list

----------------
--  periodes  --
----------------
periodes :: Eq a => [a] -> [Int]
periodes liste = [p | p <- [1..length liste-1], estPeriode p liste]

-- vim:set et sw=4 ts=4 tw=80:

