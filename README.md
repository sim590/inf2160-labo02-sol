# INF2160 - Solution du laboratoire 2

## Préparation

Afin de récupérer le fichier d'énoncé du laboratoire directement, simplement
faire:

```sh
$ make
```

Le sous-module `inf2160-exercices` sera récupéré sur gitlab avec la création de
liens symboliques appropriés.

## Solutions

Voir le fichier `solutions.md`.

<!-- vim:set et sw=4 ts=4 tw=80: -->

